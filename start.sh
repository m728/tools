/usr/sbin/service ssh start
#!/bin/bash

if pgrep ngrok > /dev/null
then
    echo "ngrok is already running"
    exit 0
else
    # start ngrok here
    /usr/local/bin/ngrok tcp 22
fi
